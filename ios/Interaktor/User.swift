//
//  User.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/14/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    var id: Int?
    var email: String?
    var password: String?
    var name: String?
    var authenticationToken: String?
    
    var establishmentId: Int?
    var establishment: Establishment?
    
    override init() {
        super.init()
    }
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        id = jsonObject["id"].int
        email = jsonObject["email"].string
        name = jsonObject["name"].string
        authenticationToken = jsonObject["authentication_token"].string
        
        establishmentId = jsonObject["establishment_id"].int
        establishment = Establishment(json: jsonObject["establishment"].object)
    }
    
    func toDictionary() -> [String: AnyObject] {
        var dict = [String: AnyObject]()
        
        if id != nil && id != 0 {
            dict.updateValue(self.id!, forKey: "id")
        }
        
        if let email = self.email {
            dict.updateValue(email, forKey: "email")
        }
        
        if let password = self.password {
            dict.updateValue(password, forKey: "password")
        }
        
        if let name = self.name {
            dict.updateValue(name, forKey: "name")
        }
        
        return dict
    }
    
}
