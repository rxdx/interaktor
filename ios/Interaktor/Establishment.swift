//
//  Establishment.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class Establishment: NSObject {

    var id: Int?
    var name: String?
    var channels = [Channel]()
    var selectedChannel: Channel?
    var selectedChannelId: Int?
    
    override init() {
        super.init()
    }
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        id = jsonObject["id"].int
        name = jsonObject["name"].string
        selectedChannel = Channel(json: jsonObject["selected_channel"].object)
        
        
        jsonObject["channels"].arrayObject?.forEach({ (channelJson) in
            channels.append(Channel(json: channelJson))
        })
    }
    
    func toDictionary() -> [String: AnyObject] {
        var dict = [String: AnyObject]()
        
        if id != nil && id != 0 {
            dict.updateValue(self.id!, forKey: "id")
        }
        
        if let selectedChannelId = self.selectedChannelId {
            dict.updateValue(selectedChannelId, forKey: "selected_channel_id")
        }
        
        return ["establishment": dict]
    }

}
