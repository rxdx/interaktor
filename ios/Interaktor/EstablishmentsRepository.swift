//
//  EstablishmentsRepository.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class EstablishmentsRepository: BaseRepository {
    
    init() {
        super.init(url: "/establishments")
    }
    
    func selectChannel(channelId: Int ,then: ThenBlock) {
        super.customPut("/select_channel/\(channelId)", then: then)
    }
}
