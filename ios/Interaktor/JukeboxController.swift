//
//  JukeboxController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class JukeboxController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var channel: Channel?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()

        getChannelDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func updateUI() {
        tableView.reloadData()
    }
    
    func showAlert(message: String) {
        let alertController = UIAlertController(title: AppName, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Entendi", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Repository
    func getChannelDetails() {
        guard let id = channel?.id else {
            return
        }
        
        ChannelsRepository().find(id) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            
            self.updateUI()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension JukeboxController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = channel?.mediaItems?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("MediaItemCell", forIndexPath: indexPath) as? MediaItemCell else {
            return UITableViewCell()
        }
        cell.mediaItem = channel?.mediaItems?[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let mediaItemId = channel?.mediaItems?[indexPath.row].id,
        channelId = channel?.id else {
            return
        }
        
        
        
        ChannelsRepository().play(channelId, mediaItemId: mediaItemId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.showAlert("Pedido enviado ;)")
        }
    }
}
