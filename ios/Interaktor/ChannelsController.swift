//
//  ChannelsController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ChannelsController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var establishment: Establishment?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChannelsController.changeChannel(_:)), name: "ChangeChannel", object: nil)
        
        navigationController?.navigationBar.topItem?.title = establishment?.name
        
        getChannel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func changeChannel(notificaiton: NSNotification) {        
        if let channelId = notificaiton.userInfo?["channel_id"] as? String {
            establishment?.channels.forEach({ (channel) in
                if String.init(format: "%d", channel.id!) == channelId {
                    if let row = establishment?.channels.indexOf(channel) {
                        let indexPath = NSIndexPath(forRow: row, inSection: 0)
                        tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .Bottom)
                    }
                }
            })
        }
    }
    
    func updateUI() {
        navigationController?.navigationBar.topItem?.title = establishment?.name
        tableView.reloadData()
    }
    
    // MARK: - Repository
    func getChannel() {
        guard let establishmentId = establishment?.id else {
            return
        }
        
        EstablishmentsRepository().find(establishmentId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.establishment = Establishment(json: response.result.value)
            self.updateUI()
        }
    }
    
    func updateChannel(channelId: Int) {
        EstablishmentsRepository().selectChannel(channelId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
        }
    }

}

// MARK: - TableView
extension ChannelsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let channels = establishment?.channels else {
            return 0
        }
        
        return channels.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let channel = establishment?.channels[indexPath.row],
            cell = tableView.dequeueReusableCellWithIdentifier("ChannelCell", forIndexPath: indexPath) as? ChannelCell else {
            return UITableViewCell()
        }
        
        cell.channel = channel
        
        if establishment?.selectedChannel?.id == channel.id {
            tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .Bottom)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let channelId = establishment?.channels[indexPath.row].id else {
            return
        }
        
        updateChannel(channelId)
    }
}
