//
//  Session.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/14/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class Session: NSObject {
    
    var user: User?
    
    var authenticationToken: String? {
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "authenticationToken")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("authenticationToken") as? String
        }
    }
    
    var email: String? {
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "email")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("email") as? String
        }
    }
    
    var uuid: String? {
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "uuid")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("uuid") as? String
        }
    }
    
    static let sharedInstance = Session()

}
