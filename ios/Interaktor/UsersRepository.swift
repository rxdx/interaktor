//
//  UsersRepository.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/14/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class UsersRepository: BaseRepository {
    
    init() {
        super.init(url: "/users")
    }
    
    func facebookLogin(facebookAccessToken: String, device: Device, then: ThenBlock) {
        var params = [String: AnyObject]()
        params.updateValue(facebookAccessToken, forKey: "facebook_access_token")
        params.updateValue(device.toDictionary(), forKey: "device")
        
        customPost("/facebook_login", parameters: params, then: then)
    }
}
