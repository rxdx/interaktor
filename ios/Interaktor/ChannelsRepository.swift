//
//  ChannelsRepository.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ChannelsRepository: BaseRepository {
    
    init() {
        super.init(url: "/channels")
    }
    
    func createScrap(channelId: Int, message: String, then: ThenBlock) {
        customPost("/\(channelId)/scrap", parameters: ["message": message], then: then)
    }
    
    func play(channelId: Int, mediaItemId: Int, then: ThenBlock) {
        customPost("/\(channelId)/play", parameters: ["media_item_id": mediaItemId], then: then)
    }
}
