//
//  SplashController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/14/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class SplashController: UIViewController {

    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addFacebookLoginButton()
        checkFacebookAccessToken()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Methods
    func addFacebookLoginButton() {
        let fbLoginButton = FBSDKLoginButton()
        fbLoginButton.center = view.center
        fbLoginButton.readPermissions = ["public_profile", "email"]
        view.addSubview(fbLoginButton)
        
        fbLoginButton.delegate = self
        
        FBSDKApplicationDelegate.sharedInstance()
    }
    
    func openEstablishmentChannels(establishmentId: Int) {
        let establishment = Establishment()
        establishment.id = establishmentId
        performSegueWithIdentifier("Admin", sender: establishment)
    }
    
    func openCustomerEstablishment(establishment: Establishment) {
        performSegueWithIdentifier("Customer", sender: establishment)
    }
    
    // MARK: - Repository
    func checkFacebookAccessToken() {
        guard let facebookToken = FBSDKAccessToken.currentAccessToken(),
            facebookAccessToken = facebookToken.tokenString else {
                return
        }
        
        let device = Device()
        device.platform = "ios"
        device.uuid = Session.sharedInstance.uuid
        UsersRepository().facebookLogin(facebookAccessToken, device: device) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            let user = User(json: response.result.value)
            Session.sharedInstance.user = user
            
            Session.sharedInstance.email = user.email
            Session.sharedInstance.authenticationToken = user.authenticationToken
            
            if let establishmentId = user.establishmentId {
                self.openEstablishmentChannels(establishmentId)
            } else {
                self.getEstablishments()
            }
        }
    }
    
    func getEstablishments() {
        EstablishmentsRepository().all { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            guard let value = response.result.value else {
                return
            }
            
            var establishments = [Establishment]()
            
            let json = JSON(value)
            json.arrayObject?.forEach({ (jsonObject) in
                let establishment = Establishment(json: jsonObject)
                establishments.append(establishment)
            })
            
            let alertController = UIAlertController(title: "Estabelecimento", message: "Selecione o estabelecimento", preferredStyle: .ActionSheet)
            establishments.forEach({ (establishment) in
                alertController.addAction(UIAlertAction(title: establishment.name, style: .Default, handler: { (action) in
                    self.openCustomerEstablishment(establishment)
                }))
            })
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let navController = segue.destinationViewController as? UINavigationController, controller = navController.topViewController as? ChannelsController {
            controller.establishment = sender as? Establishment
        }
        
        if let controller = segue.destinationViewController as? CustomerController,
            establishment = sender as? Establishment {
            controller.establishment = establishment
        }
    }
}

extension SplashController: FBSDKLoginButtonDelegate {
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("Facebook Login")
        print(result.token.tokenString)
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("Facebook Logout")
    }
}