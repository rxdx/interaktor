//
//  ScrapbookController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ScrapbookController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var channel: Channel?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getChannelDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func clickOnSendMessage(sender: AnyObject) {
        let alertController = UIAlertController(title: "Escreva sua mensagem", message: nil, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Enviar", style: .Default, handler: { (action) in
            if let message = alertController.textFields?.first?.text {
                self.sendMessage(message)
            }
        }))
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.addTarget(self, action: #selector(ScrapbookController.textFieldChanged(_:)), forControlEvents: .EditingChanged)
        }

        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Methods
    func updateUI() {
        tableView.reloadData()
    }
    
    func textFieldChanged(textField: UITextField) {
        guard let controller = presentedViewController as? UIAlertController else {
            return
        }
        
        if let message = controller.textFields?.first?.text,
            action = controller.actions.last {
            action.enabled = message.characters.count > 5
        }
    }
    
    // MARK: - Repository
    func getChannelDetails() {
        guard let id = channel?.id else {
            return
        }
        
        ChannelsRepository().find(id) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            
            self.updateUI()
        }
    }
    
    func sendMessage(message: String) {
        guard let channelId = channel?.id else {
            return
        }
        ChannelsRepository().createScrap(channelId, message: message) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            self.updateUI()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ScrapbookController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = channel?.scraps?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("ScrapCell", forIndexPath: indexPath) as? ScrapCell else {
            return UITableViewCell()
        }
        cell.scrap = channel?.scraps?[indexPath.row]
        return cell
    }
}