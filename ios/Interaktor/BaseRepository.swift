//
//  BaseRepository.swift
//  RxDx
//
//  Created by Rodrigo Dumont on 8/31/15.
//  Copyright (c) 2015 Rodrigo Dumont. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let BaseUrl = "http://172.16.2.131:3000"

class BaseRepository: NSObject {
    
    typealias ThenBlock = Response<AnyObject, NSError> -> Void
    
    enum RepositoryError: ErrorType {
        case Unknown
    }
    
    var url: String
    
    init(url: String) {
        self.url = url
    }
    
    func createHeader() -> [String: String] {
        var dict = [String: String]()
        if let token = Session.sharedInstance.authenticationToken, email = Session.sharedInstance.email  {
            dict = [
                "X-User-Token": token,
                "X-User-Email": email,
                "Content-Type": "application/json",
            ]
        }
        
        return dict
    }
    
    // MARK: - REST API
    func all(then: ThenBlock) {
        get("\(BaseUrl)\(url).json", then: then)
    }
    
    func create(parameters: [String: AnyObject], then: ThenBlock) {
        post("\(BaseUrl)\(url).json", parameters: parameters, then: then)
    }
    
    func find(identifier: Int, then: ThenBlock) {
        get("\(BaseUrl)\(url)/\(identifier).json", then: then)
    }
    
    func update(identifier: Int, parameters: [String: AnyObject], then: ThenBlock) {
        put("\(BaseUrl)\(url)/\(identifier).json", parameters: parameters, then: then)
    }
    
    func remove(identifier: Int, then: ThenBlock) {
        delete("\(BaseUrl)\(url)/\(identifier).json", then: then)
    }
    
    // MARK: - Custom Methods
    func customPost(url: String, parameters: [String: AnyObject], then: ThenBlock) {
        post("\(BaseUrl)\(self.url)\(url).json", parameters: parameters, then: then)
    }
    
    func customGet(url: String = "", parameters: [String: AnyObject]? = nil, then: ThenBlock) {
        get("\(BaseUrl)\(self.url)\(url).json", parameters: parameters, then: then)
    }
    
    func customPut(url: String = "", parameters: [String: AnyObject] = [String: AnyObject](), then: ThenBlock) {
        put("\(BaseUrl)\(self.url)\(url).json", parameters: parameters, then: then)
    }
    
    // MARK: - HTTP VERBS
    func get(url: String, parameters: [String:AnyObject]? = nil, then: ThenBlock) {
        Alamofire.request(.GET, url, parameters: parameters, headers: createHeader())
            .responseJSON { response in
                self.handleResponse(response, then: then)
        }
    }
    
    func post(url: String, parameters: [String: AnyObject], then: ThenBlock) {
        Alamofire.request(.POST, url, parameters: parameters, encoding: .JSON, headers: createHeader())
            .responseJSON { response in
                self.handleResponse(response, then: then)
        }
    }
    
    func delete(url: String, then: ThenBlock) {
        Alamofire.request(.DELETE, url, headers: createHeader())
            .responseJSON { response in
                self.handleResponse(response, then: then)
        }
    }
    
    func put(url: String, parameters: [String: AnyObject], then: ThenBlock) {
        Alamofire.request(.PUT, url, parameters: parameters, headers: createHeader())
            .responseJSON { response in
                self.handleResponse(response, then: then)
        }
    }
    
    // MARK: - Response
    func handleResponse(response: Response<AnyObject, NSError>, then: ThenBlock) {
        NSLog(response.debugDescription)
        
        switch response.result {
        case .Success:
            if let value = response.result.value {
                let json = JSON(value)
                if json["error"] != nil || json["errors"] != nil {
                    let errorDesc = json["error"] == nil ? json["errors"].description : json["error"].description
                    
                    NSLog(errorDesc)
                    
                    let result = Result<AnyObject, NSError>.Failure(Error.errorWithCode(420, failureReason: errorDesc))
                    
                    let eResult = Response(request: response.request, response: response.response, data: response.data, result: result)
                    then(eResult)
                    
                    return
                }
            }
            
            then(response)
            
        case .Failure:
            then(response)
        }
    }
}

func checkError(response: Response<AnyObject, NSError>) -> String? {
    if response.result.isSuccess {
        return nil
    }
    
    if let error = response.result.error?.userInfo["NSLocalizedFailureReason"] as? String {
        var aux = error
        aux = aux.stringByReplacingOccurrencesOfString("\n", withString: "")
        aux = aux.stringByReplacingOccurrencesOfString("[", withString: "")
        aux = aux.stringByReplacingOccurrencesOfString("]", withString: "")
        aux = aux.stringByReplacingOccurrencesOfString("\"", withString: "")
        return aux
    } else {
        return "error"
    }
}
