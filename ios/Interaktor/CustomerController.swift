//
//  CustomerController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class CustomerController: UITabBarController {
    
    var establishment: Establishment?

    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()

        getEstablishment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func updateUI() {
        var controllers = [UIViewController]()
        
        establishment?.channels.forEach { (channel) in
            guard let category = channel.category else {
                return
            }
            
            switch category {
            case "jukebox":
                guard let controller = self.storyboard?.instantiateViewControllerWithIdentifier("JukeboxController") as? JukeboxController else {
                    return
                }
                let navController = UINavigationController(rootViewController: controller)
                controller.channel = channel
               
                navController.tabBarItem.title = channel.name
                controllers.append(navController)
                
            case "scrapbook":
                guard let controller = self.storyboard?.instantiateViewControllerWithIdentifier("ScrapbookController") as? ScrapbookController else {
                    return
                }
                controller.channel = channel
                
                let navController = UINavigationController(rootViewController: controller)
                navController.tabBarItem.title = channel.name
                controllers.append(navController)
                
            default:
                break
            }
            
        }
        
        viewControllers = controllers
    }
    
    // MARK: - Repository
    func getEstablishment() {
        guard let establishmentId = establishment?.id else {
            return
        }
        
        EstablishmentsRepository().find(establishmentId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            guard let value = response.result.value else {
                return
            }
            
            self.establishment = Establishment(json: value)
            self.updateUI()
        }
    }


}
