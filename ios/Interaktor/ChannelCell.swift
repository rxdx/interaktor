//
//  ChannelCell.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    var channel: Channel? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - UIKit
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Methods
    func updateUI() {
        nameLabel.text = channel?.name
        
        if let category = channel?.category {
            switch category {
            case "news":
                categoryImageView.image = UIImage(named: "channelNews")
            case "offers":
                categoryImageView.image = UIImage(named: "channelOffer")
            case "jukebox", "playlist":
                categoryImageView.image = UIImage(named: "channelMusic")
            case "scrapbook":
                categoryImageView.image = UIImage(named: "channelChat")
            default:
                break
            }
        }
    }
}
