//
//  Channel.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class Channel: NSObject {
    
    var id: Int?
    var name: String?
    var title: String?
    var category: String?
    var mediaItems: [MediaItem]?
    var scraps: [Scrap]?

    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        id = jsonObject["id"].int
        name = jsonObject["name"].string
        title = jsonObject["title"].string
        category = jsonObject["category"].string
        
        mediaItems = [MediaItem]()
        jsonObject["media_items"].arrayObject?.forEach({ (mediaItemJsonObject) in
            mediaItems?.append(MediaItem(json: mediaItemJsonObject))
        })
        
        scraps = [Scrap]()
        jsonObject["scraps"].arrayObject?.forEach({ (scrapJsonObject) in
            scraps?.append(Scrap(json: scrapJsonObject))
        })
    }
    
}
