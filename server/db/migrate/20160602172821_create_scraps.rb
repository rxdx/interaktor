class CreateScraps < ActiveRecord::Migration
  def change
    create_table :scraps do |t|
      t.string :title
      t.string :message
      t.references :user, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
