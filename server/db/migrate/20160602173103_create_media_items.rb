class CreateMediaItems < ActiveRecord::Migration
  def change
    create_table :media_items do |t|
      t.string :title
      t.string :link
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
