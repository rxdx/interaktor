class AddEstablishmentToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :establishment, index: true, foreign_key: true
  end
end
