class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :name
      t.string :title
      t.string :category
      t.references :establishment, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_reference :establishments, :selected_channel, references: :channels, index: true
    add_foreign_key :establishments, :channels, column: :selected_channel_id
  end
end
