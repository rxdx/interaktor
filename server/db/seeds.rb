# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Establishment.create(name: "Rodrigo's Bar")

User.create(email: "rodrigo@rxdx.com.br", password: "asdqwe", establishment_id: 1)

Channel.create(name: "Notícias", title: "Canal de notícias", establishment_id: 1, category: "news")
News.create(title: "Titulo 1", content: "Conteúdo que pode ser bem longo número 1", channel_id: 1)
News.create(title: "Titulo 2", content: "Conteúdo que pode ser bem longo número 2", channel_id: 1)
News.create(title: "Titulo 3", content: "Conteúdo que pode ser bem longo número 3", channel_id: 1)

Channel.create(name: "Ofertas", title: "Canal de ofertas", establishment_id: 1, category: "offers")
Offer.create(title: "Teste 1", price: 4.20, channel_id: 2)
Offer.create(title: "Teste 2", price: 4.21, channel_id: 2)
Offer.create(title: "Teste 3", price: 4.22, channel_id: 2)

Channel.create(name: "Jukebok", title: "Canal de jukebox", establishment_id: 1, category: "jukebox")
MediaItem.create(title: "Baioque", link: "https://www.youtube.com/watch?v=xzeQQcocdlE", channel_id: 3)
MediaItem.create(title: "Mood For Ska", link: "https://www.youtube.com/watch?v=e3ciRgl8wcM", channel_id: 3)
MediaItem.create(title: "Skinheads Dem a Come", link: "http://www.youtube.com/watch?v=e5ImEGJe7Ek", channel_id: 3)

Channel.create(name: "Mural de Recados", title: "Canal de mural de recados", establishment_id: 1, category: "scrapbook")
Scrap.create(title: "Teste 1", message: "Mensagem 1", user_id: 1, channel_id: 4)
Scrap.create(title: "Teste 2", message: "Mensagem 2", user_id: 1, channel_id: 4)
Scrap.create(title: "Teste 3", message: "Mensagem 3", user_id: 1, channel_id: 4)

Establishment.first.update(selected_channel_id: 1)
