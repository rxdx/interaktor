class Channel < ActiveRecord::Base
  belongs_to :establishment

  has_many :media_items
  has_many :news
  has_many :offers
  has_many :scraps, -> { order 'created_at desc' }
end
