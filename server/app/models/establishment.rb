class Establishment < ActiveRecord::Base
  belongs_to :selected_channel, class_name: "Channel"
  has_many :users
  has_many :channels
end
