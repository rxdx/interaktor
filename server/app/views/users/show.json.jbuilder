json.extract! @user, :id, :email, :establishment_id, :created_at, :updated_at
unless @user.establishment.nil?
  json.establishment do
    json.id @user.establishment.id
    json.channels @user.establishment.channels
    json.name @user.establishment.name
    json.selected_channel @user.establishment.selected_channel
    json.created_at @user.establishment.created_at
    json.updated_at @user.establishment.updated_at
  end
end
