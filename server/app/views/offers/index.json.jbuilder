json.array!(@offers) do |offer|
  json.extract! offer, :id, :title, :price, :channel_id
  json.url offer_url(offer, format: :json)
end
