json.array!(@scraps) do |scrap|
  json.extract! scrap, :id, :title, :message, :user_id, :channel_id
  json.url scrap_url(scrap, format: :json)
end
