json.extract! @channel, :id, :name, :title, :category, :establishment_id, :created_at, :updated_at,
  :establishment, :news, :offers, :media_items, :scraps
