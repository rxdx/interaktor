class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  skip_before_filter :authenticate_user!, :only => :facebook_login
  acts_as_token_authentication_handler_for User, except: [:facebook_login]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /users/facebook_login
  def facebook_login
    facebook_access_token = params[:facebook_access_token]
    if facebook_access_token.blank?
      render json: {error: "Facebook Access Token required"}
      return
    end

    @graph = Koala::Facebook::API.new(facebook_access_token)
    profile = @graph.get_object("me", fields: ["email", "name"])

    email = profile["email"]
    if email.blank?
      render json: {error: "Email required"}
      return
    end

    @user = User.find_by(email: email)
    if @user.nil?
      @user = User.create(email: email,
        password: "LegalizeCannabis",
        name: profile["name"],
        facebook_user_id: profile["id"])
    end

    unless params["device"].nil?
      platform = params["device"]["platform"]
      uuid = params["device"]["uuid"]

      unless platform.nil? || uuid.nil?
        unless platform.empty? || uuid.empty?
          @user.devices << Device.find_or_create_by(platform: params["device"]["platform"], uuid: params["device"]["uuid"])
        end
      end
    end

    if @user.save
      render json: @user
    else
      render json: @user.errors
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email)
    end
end
