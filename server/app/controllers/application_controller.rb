class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  respond_to :json
  
  acts_as_token_authentication_handler_for User
  before_action :authenticate_user!

  APN = Houston::Client.development
  APN.certificate = File.read("./certificates/InteraktorDevPush.pem")

end
