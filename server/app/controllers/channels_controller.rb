class ChannelsController < ApplicationController
  before_action :set_channel, only: [:show, :edit, :update, :destroy]

  # GET /channels
  # GET /channels.json
  def index
    @channels = Channel.where(establishment_id: current_user.establishment_id)
  end

  # GET /channels/1
  # GET /channels/1.json
  def show
  end

  # GET /channels/new
  def new
    @channel = Channel.new
  end

  # GET /channels/1/edit
  def edit
  end

  # POST /channels
  # POST /channels.json
  def create
    @channel = Channel.new(channel_params)

    respond_to do |format|
      if @channel.save
        format.html { redirect_to @channel, notice: 'Channel was successfully created.' }
        format.json { render :show, status: :created, location: @channel }
      else
        format.html { render :new }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /channels/1
  # PATCH/PUT /channels/1.json
  def update
    respond_to do |format|
      if @channel.update(channel_params)
        format.html { redirect_to @channel, notice: 'Channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @channel }
      else
        format.html { render :edit }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channels/1
  # DELETE /channels/1.json
  def destroy
    @channel.destroy
    respond_to do |format|
      format.html { redirect_to channels_url, notice: 'Channel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /channels/:id/scrap
  def create_scrap
    if params["message"].nil?
      render json: { error: "message required" }
      return
    end

    @channel = Channel.find(params["id"])
    if @channel.nil?
      render json: { error: "channel not found" }
      return
    end

    scrap = Scrap.create(message: params["message"], user_id: current_user.id)
    @channel.scraps << scrap

    @channel.establishment.users.each do |user|
      user.devices.each do |device|
        if device.platform == "tvos"
          token = device.uuid
          notification = Houston::Notification.new(device: token)
          notification.alert = current_user.email + " enviou um novo recado"

          notification.category = "NewScrap"
          notification.custom_data = {scrap_id: scrap.id}

          APN.push(notification)
        end
      end
    end

    if @channel.save
      render :show
    else
      render json: @channel.errors
    end

  end

  # POST /channels/:id/play
  def play
    if params["media_item_id"].nil?
      render json: { error: "media_item_id required" }
      return
    end

    @channel = Channel.find(params["id"])
    if @channel.nil?
      render json: { error: "channel not found" }
      return
    end

    @channel.establishment.users.each do |user|
      user.devices.each do |device|
        if device.platform == "tvos"
          token = device.uuid
          notification = Houston::Notification.new(device: token)
          notification.alert = current_user.email + " tocou uma música"

          notification.category = "PlayMediaItem"
          notification.custom_data = {media_item_id: params["media_item_id"]}

          APN.push(notification)
        end
      end
    end

    render json: @channel

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_params
      params.require(:channel).permit(:name, :title, :category, :establishment_id)
    end
end
