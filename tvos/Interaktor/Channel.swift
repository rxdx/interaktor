//
//  Channel.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/2/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class Channel: NSObject {

    var id: Int?
    var name: String?
    var title: String?
    var category: String?
    
    var news: [News]?
    var offers: [Offer]?
    var mediaItems: [MediaItem]?
    var scraps: [Scrap]?
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        id = jsonObject["id"].int
        name = jsonObject["name"].string
        title = jsonObject["title"].string
        category = jsonObject["category"].string
        
        news = [News]()
        jsonObject["news"].arrayObject?.forEach({ (newsJsonObject) in
            news?.append(News(json: newsJsonObject))
        })
        
        offers = [Offer]()
        jsonObject["offers"].arrayObject?.forEach({ (offerJsonObject) in
            offers?.append(Offer(json: offerJsonObject))
        })
        
        mediaItems = [MediaItem]()
        jsonObject["media_items"].arrayObject?.forEach({ (mediaItemJsonObject) in
            mediaItems?.append(MediaItem(json: mediaItemJsonObject))
        })
        
        scraps = [Scrap]()
        jsonObject["scraps"].arrayObject?.forEach({ (scrapJsonObject) in
            scraps?.append(Scrap(json: scrapJsonObject))
        })
    }

}
