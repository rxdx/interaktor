//
//  Offer.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/3/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class Offer: NSObject {

    var title: String?
    var price: Double?
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        title = jsonObject["title"].string
        
        if let price = jsonObject["price"].string {
            self.price = Double(price)
        }
        
    }
    
}
