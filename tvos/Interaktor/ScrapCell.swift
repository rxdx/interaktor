//
//  ScrapCell.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/6/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ScrapCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    
    var scrap: Scrap? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - UIKit
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Methods
    func updateUI() {
        message.text = scrap?.message
    }

}
