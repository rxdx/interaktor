//
//  MediaItem.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/3/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class MediaItem: NSObject {

    var id: Int?
    var title: String?
    var link: String?
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        id = jsonObject["id"].int
        title = jsonObject["title"].string
        link = jsonObject["link"].string
    }

}
