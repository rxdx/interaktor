//
//  ViewController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 5/31/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import FBSDKTVOSKit

class SplashController: UIViewController {

    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addFacebookLoginButton()
        checkFacebookAccessToken()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Repository
    func checkFacebookAccessToken() {
        guard let facebookToken = FBSDKAccessToken.currentAccessToken(),
            facebookAccessToken = facebookToken.tokenString else {
                return
        }
        
        let device = Device()
        device.platform = "tvos"
        device.uuid = Session.sharedInstance.uuid
        
        UsersRepository().facebookLogin(facebookAccessToken, device: device) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            let user = User(json: response.result.value)
            Session.sharedInstance.user = user
            
            Session.sharedInstance.email = user.email
            Session.sharedInstance.authenticationToken = user.authenticationToken
            
            self.presentTabBar()
        }
    }
    
    // MARK: - Methods
    func addFacebookLoginButton() {
        let button = FBSDKDeviceLoginButton(frame: CGRect.zero)
        button.readPermissions = ["email"]
        button.center = self.view.center
        
        button.delegate = self
        
        self.view.addSubview(button)
    }
    
    func presentTabBar() {
        let controller = TabBarController()
        presentViewController(controller, animated: true, completion: nil)
    }

}

extension SplashController: FBSDKDeviceLoginButtonDelegate {
    func deviceLoginButtonDidCancel(button: FBSDKDeviceLoginButton) {
    }
    func deviceLoginButtonDidLogIn(button: FBSDKDeviceLoginButton) {
        checkFacebookAccessToken()
    }
    func deviceLoginButtonDidLogOut(button: FBSDKDeviceLoginButton) {
    }
    func deviceLoginButtonDidFail(button: FBSDKDeviceLoginButton, error: NSError) {
    }
}

