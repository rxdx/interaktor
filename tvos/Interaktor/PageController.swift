//
//  PageController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/3/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class PageController: UIPageViewController {
    
    var channel: Channel?
    var controllers = [UIViewController]()
    var selectedPageIndex = 0
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        getChannelDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func updateUI() {
        guard let category = channel?.category else {
            return
        }
        
        switch category {
        case "news":
            showNews()
        case "offers":
            showOffers()
        default:
            break
        }
        
        startTimer()
    }
    
    func startTimer() {
        let timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(PageController.nextPage), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    func nextPage() {
        selectedPageIndex = (selectedPageIndex + 1) % controllers.count
        setViewControllers([controllers[selectedPageIndex]], direction: .Forward, animated: true, completion: nil)
    }
    
    func showNews() {
        controllers = [UIViewController]()
        channel?.news?.forEach({ (news) in
            guard let controller = storyboard?.instantiateViewControllerWithIdentifier("NewsController") as? NewsController else {
                return
            }
            
            controller.news = news
            controllers.append(controller)
        })
        
        if controllers.count > 0 {
            setViewControllers([controllers.first!], direction: .Forward, animated: true, completion: nil)
        }
    }
    
    func showOffers() {
        controllers = [UIViewController]()
        channel?.offers?.forEach({ (offer) in
            guard let controller = storyboard?.instantiateViewControllerWithIdentifier("OfferController") as? OfferController else {
                return
            }
            
            controller.offer = offer
            controllers.append(controller)
        })
        
        if controllers.count > 0 {
            setViewControllers([controllers.first!], direction: .Forward, animated: true, completion: nil)
        }
    }
    
    // MARK: - Repository
    func getChannelDetails() {
        guard let id = channel?.id else {
            return
        }
        
        ChannelsRepository().find(id) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            
            self.updateUI()
        }
    }
    

}

// MARK: UIPageViewControllerDataSource
extension PageController: UIPageViewControllerDataSource {
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        guard let index = controllers.indexOf(viewController) else {
            return nil
        }
        
        if index == 0 {
            return controllers.last
        }
        
        return controllers[index-1]
    }
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        guard let index = controllers.indexOf(viewController) else {
            return nil
        }
        
        if index == controllers.count-1 {
            return controllers.first
        }
        
        return controllers[index+1]
    }
    
}