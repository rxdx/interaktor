//
//  ScrapsRepository.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/17/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ScrapsRepository: BaseRepository {

    init() {
        super.init(url: "/scraps")
    }
}
