//
//  JukeboxController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/3/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import HCYoutubeParser
import AVKit

class JukeboxController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: UIView!
    
    var channel: Channel?
    var avPlayerLayer: AVPlayerLayer?
    
    var avPlayer: AVPlayer?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(JukeboxController.playMediaItem(_:)), name: "PlayMediaItem", object: nil)

        getChannelDetails()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopPlayer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func updateUI() {
        tableView.reloadData()
    }
    
    func playMediaItem(notificaiton: NSNotification) {
        if let mediaItemId = notificaiton.userInfo?["media_item_id"] as? String {
            if let intValue = Int(mediaItemId) {
                self.channel?.mediaItems?.forEach({ (mediaItem) in
                    if mediaItem.id == intValue {
                        if let row = channel?.mediaItems?.indexOf(mediaItem) {
                            let indexPath = NSIndexPath(forRow: row, inSection: 0)
                            tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .Bottom)
                            openLink(mediaItem)
                        }
                    }
                })
            }
        }
    }
    
    // MARK: - Repository
    func getChannelDetails() {
        guard let id = channel?.id else {
            return
        }
        
        ChannelsRepository().find(id) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            
            self.updateUI()
        }
    }
    
    func openLink(mediaItem: MediaItem) {
        stopPlayer()
        
        guard let link = mediaItem.link else {
            return
        }
        
        if let videos = HCYoutubeParser.h264videosWithYoutubeURL(NSURL(string: link)) {
            guard let urlString = videos["medium"] as? String, nsUrl = NSURL(string: urlString) else {
                return
            }
            
            let asset = AVAsset(URL: nsUrl)
            let avPlayerItem = AVPlayerItem(asset:asset)
            avPlayer = AVPlayer(playerItem: avPlayerItem)
            avPlayerLayer  = AVPlayerLayer(player: avPlayer)
            avPlayerLayer?.frame = CGRectMake(0, 0, playerView.frame.width, playerView.frame.height);
            playerView.layer.addSublayer(avPlayerLayer!)
            avPlayer?.play()
        } else {
            print("Erro ao tocar: \(mediaItem.title)")
        }
    }
    
    func stopPlayer() {
        avPlayer?.pause()
        avPlayerLayer?.removeFromSuperlayer()
    }
}

extension JukeboxController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = channel?.mediaItems?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("MediaItemCell", forIndexPath: indexPath) as? MediaItemCell else {
            return UITableViewCell()
        }
        cell.mediaItem = channel?.mediaItems?[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let mediaItem = channel?.mediaItems?[indexPath.row] else {
            return
        }
    
        openLink(mediaItem)
    }
}
