//
//  ChannelsRepository.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/2/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ChannelsRepository: BaseRepository {
    
    init() {
        super.init(url: "/channels")
    }

}
