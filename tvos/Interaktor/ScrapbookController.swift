//
//  ScrapbookController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/6/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class ScrapbookController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var channel: Channel?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ScrapbookController.newScrap(_:)), name: "NewScrap", object: nil)

        getChannelDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func newScrap(notificaiton: NSNotification) {
        if let scrapId = notificaiton.userInfo?["scrap_id"] as? String {
            if let intValue = Int(scrapId) {
                getScrap(intValue)
            }
        }
    }
    
    func updateUI() {
        tableView.reloadData()
    }
    
    // MARK: - Repository
    func getChannelDetails() {
        guard let id = channel?.id else {
            return
        }
        
        ChannelsRepository().find(id) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            self.channel = Channel(json: response.result.value)
            
            self.updateUI()
        }
    }
    
    func getScrap(scrapId: Int) {
        ScrapsRepository().find(scrapId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            let scrap = Scrap(json: response.result.value)
            self.channel?.scraps?.insert(scrap, atIndex: 0)
            self.updateUI()
        }
    }
    
}

extension ScrapbookController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = channel?.scraps?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("ScrapCell", forIndexPath: indexPath) as? ScrapCell else {
            return UITableViewCell()
        }
        cell.scrap = channel?.scraps?[indexPath.row]
        return cell
    }
}