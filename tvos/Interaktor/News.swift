//
//  News.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/3/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class News: NSObject {
    
    var title: String?
    var content: String?
    
    init(json: AnyObject?) {
        super.init()
        
        guard let json = json else {
            return
        }
        
        let jsonObject = JSON(json)
        
        title = jsonObject["title"].string
        content = jsonObject["content"].string
        
    }

}
