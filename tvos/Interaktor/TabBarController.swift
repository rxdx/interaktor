//
//  TabBarController.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/2/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit
import SwiftyJSON

class TabBarController: UITabBarController {
    
    var establishment: Establishment?
    var selectedChannelController: UIViewController?
    
    // MARK: - UIKit
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TabBarController.changeChannel(_:)), name: "ChangeChannel", object: nil)
        
        getEstablishment()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func changeChannel(notificaiton: NSNotification) {
        if let channelId = notificaiton.userInfo?["channel_id"] as? String {
            establishment?.channels.forEach({ (channel) in
                if String.init(format: "%d", channel.id!) == channelId {
                    if let index = establishment?.channels.indexOf(channel) {
                        self.selectedChannelController = self.viewControllers?[index]
                        self.selectedViewController = self.selectedChannelController
                    }
                }
            })
        }
    }
    
    func updateUI() {
        var controllers = [UIViewController]()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        establishment?.channels.forEach { (channel) in
            guard let category = channel.category else {
                return
            }
            
            switch category {
            case "jukebox":
                guard let controller = storyboard.instantiateViewControllerWithIdentifier("JukeboxController") as? JukeboxController else {
                    return
                }
                controller.channel = channel
                controller.tabBarItem.title = channel.name
                controllers.append(controller)
                
                if channel.id == establishment?.selectedChannel?.id {
                    selectedChannelController = controller
                }
            case "news", "offers":
                guard let controller = storyboard.instantiateViewControllerWithIdentifier("PageController") as? PageController else {
                    return
                }
                
                controller.channel = channel
                controller.tabBarItem.title = channel.name
                controllers.append(controller)
                
                if channel.id == establishment?.selectedChannel?.id {
                    selectedChannelController = controller
                }
            case "scrapbook":
                guard let controller = storyboard.instantiateViewControllerWithIdentifier("ScrapbookController") as? ScrapbookController else {
                    return
                }
                controller.channel = channel
                controller.tabBarItem.title = channel.name
                controllers.append(controller)
                
                if channel.id == establishment?.selectedChannel?.id {
                    selectedChannelController = controller
                }
            default:
                break
            }
            
        }
        
        viewControllers = controllers
        if let controller = selectedChannelController {
            selectedViewController = controller
        }
    }
    
    // MARK: - Repository
    func getEstablishment() {
        guard let establishmentId = Session.sharedInstance.user?.establishmentId else {
            return
        }
        EstablishmentsRepository().find(establishmentId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            
            guard let value = response.result.value else {
                return
            }
            
            self.establishment = Establishment(json: value)
            self.updateUI()
        }
    }
}

extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        guard let index = viewControllers?.indexOf(viewController),
            channelId = establishment?.channels[index].id,
            establishmentId = establishment?.id else {
                return
            }
        
        let aux = Establishment()
        aux.id = establishmentId
        aux.selectedChannelId = channelId
        
        EstablishmentsRepository().selectChannel(channelId) { (response) in
            if let error = checkError(response) {
                print(error)
                return
            }
            print("Did select channel \(channelId)")
        }
    }
}
