//
//  MediaItemCell.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/6/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class MediaItemCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!

    var mediaItem: MediaItem? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - UIKit
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Methods
    func updateUI() {
        title.text = mediaItem?.title
    }
}
