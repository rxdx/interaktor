//
//  Device.swift
//  Interaktor
//
//  Created by Rodrigo Dumont on 6/16/16.
//  Copyright © 2016 Rodrigo Dumont. All rights reserved.
//

import UIKit

class Device: NSObject {
    
    var id: Int?
    var platform: String?
    var uuid: String?
    
    func toDictionary() -> [String: AnyObject] {
        var dict = [String: AnyObject]()
        
        if id != nil && id != 0 {
            dict.updateValue(id!, forKey: "id")
        }
        
        if let platform = platform {
            dict.updateValue(platform, forKey: "platform")
        }
        
        if let uuid = uuid {
            dict.updateValue(uuid, forKey: "uuid")
        }
        
        return dict
    }

}
